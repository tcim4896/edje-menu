/*
    edje menu, just the push menu with levels
    wonder how we get in depth with this structure type
    flat/hyrar?

	<div class="header">
  <div class="btn btn-default">
    <a class="menu-toggle" (click)="_menu.action('toggle', 0)">
      {{_menu.opened.length > 0 ? '⊗' : '≡' }}
    </a>
  </div>
  <div class="btn btn-default">
    <a class="menu-toggle" (click)="_menu.toggleOrientation()">
      {{_menu.orientation === 'top' ? '◄' : '▲'}}
    </a>
  </div>
</div>

*/
RegisterService({
    name:"menu",
  	opened:[],
    offset:30,
    size:200,
    orientation:'left',
    structure:[
        {
        text: "Dictea",
        event: {type:"open",path:0},
        list: [ // if list defined gen level and below are items and levels are not nested, sure?
            {
                text: "Mangler",
                event: {type:"route",path:"mangler"}
            },
            {
                text: "Langcheckup",
                event: {type:"open",path:1},// what about this path thing, should be more logical if gen.
                list: [
                    {
                        text: "Dept",
                        event: {type:"route",path:"Dept"}            
                    }
                ]
            }, // ,
        ],
    },
    ]
})



o({id:"header",class:"header",siblings:[
  o({id:"open-btn",text:"+",class:"btn"})
]})

e(s("open-btn"),"click",function(){
  var siblings=[];
  for(let i=0;i<10;i++){
    siblings.push(o({id:i,class:"item",text:"item"+i}))
  }
  b(root,o({id:"level",class:"level left",text:"items",siblings}))

})


// for(let level of _.menu.structure){
//   if(level.list.length>0){
//     for(let item of level.list){
//       console.log(item)
//       b(s("level"),o({id:item.text,text:item.text,class:"item"}))
//     }
//   }
// }
function isOverlapped(id) {
    return this.opened.indexOf(id) !== (this.opened.length-1);
}

b(root,s("header"))
function domConvert(elm){
	return {
		element:elm,
		text:elm.textContent
	}
}
cl(domConvert(s("header")))
function toggleOrientation(){
    // this.orientation === 'top' ?
    //   this.orientation = 'left' :
    //   this.orientation = 'top';
}
function px(value){
    return value + "px";// not true
  }
  
function action(type, path){
    var self = this;
    var levels = self.structure.levels;

    switch(type){
      case "toggle":
        if(self.opened.indexOf(path) === -1){
          //push one level id to opened if not opened
          self.opened.push(path);
        }else{
          path === 0 ? this.opened = [] : {}
        }
        adaptSize();
      break;
      case "closeUntil":
         // close all untill level
        closeUntil(path);
        adaptSize();
      break;
      case "link":
        self.opened = [];
        adaptSize();
        this._router.navigateByUrl(path);
      break;
    }
    // helper functions
	function closeUntil(id){
		// close all levels until id
		console.log(self.opened);
		self.opened = self.opened.slice(0, self.opened.indexOf(id)+1);
		console.log(self.opened, 'close until', id);
		
	}

	function adaptSize(){
		var sizes = [];
		// set size of every closed level to 0px;
		levels.map(level => {
		!self.opened.includes(level.id) ?
			level.size = "0px" : {}
		return level;
		});
		// calculate sizes of levels
		sizes = self.opened.map((level, index) => {
		return self.px(self.size + (index * self.offset));
		}).reverse();
		// assign sizes to levels that are open
		self.opened.map((level, index) => {
		levels[level].size = sizes[index];
		});
		console.log("adaptSize()", self.opened, sizes)
	}
}
function Router(pageId){ // handler for request and routes
    /*
        dynamic switcher(in case of registerRoute) so no switch statement..

    */
    switch(pageId){
        case "mangler":
            b(root,s("mangler"))
        break;
        default:
            b(root,s("home"))
        break;
    }
}

RegisterService({
    name:Router,
    router:Router
})
// 



// toggle menu
function toggle(id){
	for(let item of _.menu.structure){
		if(item.event.type=="open"){
			s("level"+item.event.path).style.height="calc(0%)";
		}
	}
	if(typeof id!=="undefined"){
		if(_.menu.open.id==id&&_.menu.open.open==true){
		_.menu.open={id,open:false};
			s("level"+id).style.height="0px";
		}else{
			_.menu.open={id,open:true};
			s("level"+id).style.height="auto";
		}
	}

}

function edjeMenu(structure){
    for(let level of structure){
		o({id:level.text})//!!! spawn at event also possible?
		for(let item of level){
			b(o({id:item.text}),s(level.text))// kind of the idea, 
		}
    }
}

// function drawMenu(menuStructure){
// 	function menuAction(menuItem){
// 		switch(menuItem.event.type){
// 			case "open":
// 				toggle(menuItem.event.path);
// 			break;
// 			case "close":
// 				_.menu.open.open=false;
// 				toggle()
// 			break;
// 			case "route":
// 				Router(menuItem.event.path);// as a service // component missing correct
// 				_.menu.open.open=false;
// 				toggle();
// 			break;
// 			case "ref":
// 				window.open(menuItem.event.path)
// 			break;
// 			default:
// 				cl("No matching route..")
// 			break;
// 		}
// 	}

// 	b(root,o({id:"menu",class:"menu"}))

// 	for(let menuItem of menuStructure){// include huh?
// 		b(s("menu"),e(o({class:"item", text:menuItem.text}),"click",function(){		
// 			menuAction(menuItem)
// 		}))
// 		function drawLevel(menuItem,rows=2){
// 			const list=menuItem.list
// 			const len=list.length;
		
// 			const divs=Math.ceil(len/rows);
// 			let itemIndex=0;rows=3;
// 			if(menuItem.event.type=="open"){
// 				b(root,o({id:"level"+menuItem.event.path, class:"level", siblings:[
// 					// o({class:"bar", text:menuItem.text,siblings:[
// 					// 	e(o({class:"close-btn",text:"x"}),"mousedown",()=> menuAction(menuItem))
// 					// 	]})
// 					   ]})
// 				)
// 			}
// 			cl(divs,len,menuItem)
// 			for(let i=0;i<divs;i++){
// 				// create division
// 				b(s("level"+menuItem.event.path),o({id:"division"+i,class:"division"}))

// 				for(let x=0;x<rows;x++){
// 					let li=menuItem.list[itemIndex];
// 					if(typeof li=="object"){
// 						if(menuItem.event.type=="open"){
// 							// create level
// 							itemIndex++;
// 							b(s("division"+i),e(o({id:"level"+li.event.path,class:"item", text:li.text}),"click",function(){
// 								menuAction(li)
// 							}))					
// 						}else{
// 							itemIndex++;
// 							b(s("division"+i),e(o({class:"item", text:li.text}),"click",function(){
// 								menuAction(li)
// 							}))
// 						}						
// 					}else{
// 						b(s("division"+i),o({class:"empty",text:"hello"}))
// 					}
	
// 				}
// 			}

// 		}
// 		drawLevel(menuItem);
// 	}

// 	e(document.body,"mousedown",function(){
// 		if(_.menu.open.open==true){
// 			if(_.y<s("level"+_.menu.open.id).offsetTop){
// 				menuAction({event:{type:"close"}})
// 			}
// 		}

// 	})
// }

// drawMenu(_.menu.structure);