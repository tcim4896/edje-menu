import { Injectable } from '@angular/core';
import {Structure} from '../shared/menu-structure';
import { identifierModuleUrl } from '@angular/compiler';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public structure;
  public opened;
  public offset;
  public size;
  public orientation;

  constructor(public _router: Router) { 
    this.structure = Structure;
    this.structure.levels.map(level => {
      level.size = "0px";
      return level;
    })
    this.opened = [];
    this.offset = 30;
    this.size = 200;
    this.orientation = 'top';
  }
  
  isOverlapped(id) {
    return this.opened.indexOf(id) !== (this.opened.length-1);
  }

  toggleOrientation(){
    this.orientation === 'top' ?
      this.orientation = 'left' :
      this.orientation = 'top';
  }

  getList(id, structure){
    return structure.lists[id];
  }

  px(value){
    return value + "px";
  }
  
  action(type, path){
    var self = this;
    var levels = self.structure.levels;

    switch(type){
      case "toggle":
        if(self.opened.indexOf(path) === -1){
          //push one level id to opened if not opened
          self.opened.push(path);
        }else{
          path === 0 ? this.opened = [] : {}
        }
        adaptSize();
      break;
      case "closeUntil":
         // close all untill level
        closeUntil(path);
        adaptSize();
      break;
      case "link":
        self.opened = [];
        adaptSize();
        this._router.navigateByUrl(path);
      break;
    }
    // helper functions
    function closeUntil(id){
      // close all levels until id
      console.log(self.opened);
      self.opened = self.opened.slice(0, self.opened.indexOf(id)+1);
      console.log(self.opened, 'close until', id);
      
    }

    function adaptSize(){
      var sizes = [];
      // set size of every closed level to 0px;
      levels.map(level => {
        !self.opened.includes(level.id) ?
          level.size = "0px" : {}
        return level;
      });
      // calculate sizes of levels
      sizes = self.opened.map((level, index) => {
        return self.px(self.size + (index * self.offset));
      }).reverse();
      // assign sizes to levels that are open
      self.opened.map((level, index) => {
        levels[level].size = sizes[index];
      });
      console.log("adaptSize()", self.opened, sizes)
    }
  }
}
